import pygame
#import tkinter
import shapes
import Intro as intro

from pygame.image import load as get_image
#import grey_shape
from pygame.locals import *
# main.py

pygame.init()
# because i may get confused with Swift Syntax and Python Syntax.
def main_func():
  true = True
  clicked_shape = 0
  false = False
  print("Build Completed")
  shape1_rect = (400, 670)
  shape2_rect = (500, 670)
  shape3_rect = (600, 670)
  
  gshape1_rect = (600, 103)
  gshape2_rect = (400, 80)
  gshape3_rect = (290, 200)

  bg = pygame.image.load("background.png")
  screen = pygame.display.set_mode((1366, 768))
  shape = shapes.Shape("shape1.png", shape1_rect)
  shape2 = shapes.Shape("shape2.png", shape2_rect)
  shape3 = shapes.Shape("shape3.png", shape3_rect)
  gShape = shapes.Shape("shape1grey.png", gshape1_rect)
  gShape2 = shapes.Shape("shape2grey.png", gshape2_rect)
  gShape3 = shapes.Shape("shape3grey.png", gshape3_rect)
  all_sprites = pygame.sprite.Group()
  shape_group = pygame.sprite.Group()
  grey_sh_group = pygame.sprite.Group()
  grey_sh_group.add(gShape)
  grey_sh_group.add(gShape2)
  grey_sh_group.add(gShape3)
  gg1 = pygame.sprite.Group()
  gg1.add(gShape)
  gg2 = pygame.sprite.Group()
  gg2.add(gShape2)
  gg3 = pygame.sprite.Group()
  gg3.add(gShape3)
  shape_group.add(shape)
  shape_group.add(shape2)
  shape_group.add(shape3)
  on_grey_shape = [False]
  #Add grey shapes first. Always.
  all_sprites.add(grey_sh_group)
  all_sprites.add(shape_group)

  clock = pygame.time.Clock()
  running = True

  while running == True:
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
            pygame.quit()
            quit()
        if event.type == MOUSEBUTTONDOWN:
              # Set the x, y postions of the mouse click
              x, y = event.pos
              
              if shape.rect.collidepoint(x, y):
                shape.shapeCmouse = true
                shape.drop_clicked()
              if pygame.sprite.spritecollideany(shape, gg1):
                gShape.assign_to_image("shape1.png")
                shape.kill()

                

              if shape2.rect.collidepoint(x, y):
                shape2.shapeCmouse = true
                shape2.drop_clicked()
              if pygame.sprite.spritecollideany(shape2, gg2):
                gShape2.assign_to_image("shape2.png")
                shape2.kill()

              if shape3.rect.collidepoint(x, y):
                shape3.shapeCmouse = true
                shape3.drop_clicked()
              if pygame.sprite.spritecollideany(shape3, gg3):
                gShape3.assign_to_image("shape3.png")
                shape3.kill()
                
    if shape.shapeCmouse == true:
      shape.move_by_mouse()

    if shape2.shapeCmouse == true:
      shape2.move_by_mouse()

    if shape3.shapeCmouse == true:
      shape3.move_by_mouse()
    

    
    screen.blit(bg, (0, 0))
    for Gsprite in grey_sh_group:
      screen.blit(Gsprite.image, (Gsprite.rect))
      Gsprite.update()
    for sprite in shape_group:
      
      screen.blit(sprite.image, (sprite.rect))
    pygame.display.flip()
    clock.tick(90)

intro.intro(main_func)         
      
