import pygame
#import tkinter
from pygame.image import load as get_image
# Shape.py

"""Classes are like items that have properties.
Under here pygame.sprite.Sprite in the parenthesis, the class Shape
inhereites the properties from pygame.sprite.Sprite.
"""
class Shape(pygame.sprite.Sprite):
    def __init__(self, img, rect):
        super(Shape, self).__init__()
        """ self just shows that the variable belongs to the class, in this
case, Shape. An example would be a dog, self.noise = 'bark', meaning it's noise
is 'bark'."""
        self.image = get_image(img)
        self.rect = self.image.get_rect(center=rect)
        self.orect = rect
        self.shapeCmouse = False
        self.gravity = 0
        self.frictionx = 0

    def move_by_mouse(self):
          self.rect = self.image.get_rect(center=pygame.mouse.get_pos())

    def drop_clicked(self):
        if self.rect.center[1] >= 100 and self.rect.center[0] >= 700:
            self.rect = self.image.get_rect(center=self.orect)
            self.shapeCmouse = False
        else:
            self.shapeCmouse = True
    def assign_to_image(self, new_image):
        self.image = get_image(new_image)
    def update(self):
        if self.rect.center[1] <= 200:
            self.gravity = 6
        elif self.rect.center[1] >= 680:
            
            self.gravity = -6
        
        
    
        self.rect.move_ip(self.frictionx, self.gravity)
       
