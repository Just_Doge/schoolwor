
import pygame
import random

def text_objects(text, font):
    BLACK = (0, 0, 0)
    textSurface = font.render(text, True, BLACK)
    return textSurface, textSurface.get_rect()

def button(msg,x,y, w, h, ac, screen, ic,action=None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    if x+w > mouse[0] > x and y+h > mouse[1] > y:
        pygame.draw.rect(screen, ac,(x,y,w,h))
        if click[0] == 1 and action != None:
            action()         
    else:
        pygame.draw.rect(screen, ic,(x,y,w,h))
    smallText = pygame.font.SysFont("comicsansms",20)
    textSurf, textRect = text_objects(msg, smallText)
    textRect.center = ( (x+(w/2)), (y+(h/2)) )
    screen.blit(textSurf, textRect)

def intro(action2):
    pygame.init()
    running = True
    screen = pygame.display.set_mode((1366, 768))
    logo = pygame.image.load("tessLogo.png")
    color1 = 1
    color2 = 2
    color3 = 3
    colors = (color1, color2, color3)
    clock = pygame.time.Clock()


    while running == True:
        color1 += 1
        color2 += 1
        color3 += 1
        if color1 >= 250:
            color1 = 3
        if color2 >= 250:
            color2 = 7
        if color3 >= 250:
            color3 = 9
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                quit()

                

            screen.fill((color1, color2, color3))
            button("Let\'s Go!", 300, 568, 500, 300, (255, 4, 1), screen, (0, 255, 0), action2)
            screen.blit(logo, ( 300,  200))
            pygame.display.flip()
            clock.tick(90)


            
